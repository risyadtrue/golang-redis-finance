package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-redis/redis"
	"go-redis-finance/controller/dto"
	"go-redis-finance/helper"
	"go-redis-finance/repository"
	"net/http"
	"sort"
	"time"
)

func BulkCreatePengeluaran(writer http.ResponseWriter, request *http.Request) {
	keys := chi.URLParam(request, "keys")
	field := "pengeluaran"

	var dataPengeluaran dto.UserPengeluaranRequest
	err := json.NewDecoder(request.Body).Decode(&dataPengeluaran)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	var totalPengeluaranInput int
	for i, data := range dataPengeluaran.Pengeluaran {
		dataPengeluaran.Pengeluaran[i].Tanggal = time.Now()
		totalPengeluaranInput = totalPengeluaranInput + data.Jumlah
	}

	dataPengeluaran.TotalPengeluaran = totalPengeluaranInput

	var dataPengeluaranRedis dto.UserPengeluaranRequest
	err = repository.RedisHMGet(keys, field, &dataPengeluaranRedis)
	if err != nil {
		if err == redis.Nil {
			// harusnya connect ke database
			mapPemasukan := make(map[string]interface{})
			dataByes, err := json.Marshal(dataPengeluaran)
			mapPemasukan[field] = string(dataByes)
			err = repository.RedisHMSet(keys, mapPemasukan, time.Minute*10)
			if err != nil {
				helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
				return
			}
		}
	}

	for _, data := range dataPengeluaran.Pengeluaran {
		dataPengeluaranRedis.Pengeluaran = append(dataPengeluaranRedis.Pengeluaran, data)
	}

	sort.Sort(dto.SlicePengeluaran(dataPengeluaranRedis.Pengeluaran))

	var totalPengeluaran int
	for _, data := range dataPengeluaranRedis.Pengeluaran {
		totalPengeluaran = totalPengeluaran + data.Jumlah
	}

	dataPengeluaranRedis.TotalPengeluaran = totalPengeluaran

	mapPemasukan := make(map[string]interface{})
	dataByes, err := json.Marshal(dataPengeluaranRedis)
	mapPemasukan[field] = string(dataByes)
	err = repository.RedisHMSet(keys, mapPemasukan, time.Minute*10)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(writer, request, dataPengeluaran)

}

func GetPengeluaran(writer http.ResponseWriter, request *http.Request) {
	keys := chi.URLParam(request, "keys")
	field := "pengeluaran"

	var redisDataPengeluaran dto.UserPengeluaranRequest
	err := repository.RedisHMGet(keys, field, &redisDataPengeluaran)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	// mapping keperluan pengeluaran
	mapDataPengeluaran := make(map[string][]dto.Pengeluaran)
	var dataKeperluan []dto.Pengeluaran
	for _, data := range redisDataPengeluaran.Pengeluaran {
		if _, found := mapDataPengeluaran[data.Keperluan]; !found {
			dataKeperluan = []dto.Pengeluaran{}
		}

		dataKeperluan = append(dataKeperluan, data)

		mapDataPengeluaran[data.Keperluan] = dataKeperluan
	}

	pengeluaranResponse := dto.UserPengeluaranResponse{
		CatatanPengeluaran: mapDataPengeluaran,
		TotalPengeluaran:   redisDataPengeluaran.TotalPengeluaran,
	}

	helper.HttpResponseSuccess(writer, request, pengeluaranResponse)

}
