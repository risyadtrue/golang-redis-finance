package dto

import "time"

type Pemasukan struct {
	Tipe    string    `json:"tipe"`
	Jumlah  int       `json:"jumlah"`
	Sumber  string    `json:"sumber"`
	Tanggal time.Time `json:"tanggal"`
}

// sort struct
type SlicePemasukan []Pemasukan

func (value SlicePemasukan) Len() int {
	return len(value)
}

func (value SlicePemasukan) Less(i, j int) bool {
	return value[i].Tipe < value[j].Tipe
}

func (value SlicePemasukan) Swap(i, j int) {
	value[i], value[j] = value[j], value[i]
}

type UserPemasukanRequest struct {
	Pemasukan      []Pemasukan `json:"pemasukan"`
	TotalPemasukan int         `json:"total_pemasukan"`
}

type UserPemasukanResponse struct {
	CatatanPemasukan interface{} `json:"catatan_pemasukan"`
	TotalPemasukan   int         `json:"total_pemasukan"`
}
