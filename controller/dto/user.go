package dto

type UserDataAll struct {
	Pengeluaran      []Pengeluaran `json:"pengeluaran"`
	TotalPengeluaran int           `json:"total_pengeluaran"`
	Pemasukan        []Pemasukan   `json:"pemasukan"`
	TotalPemasukan   int           `json:"total_pemasukan"`
}

type UserSaloResponse struct {
	Nama  string `json:"nama"`
	Saldo int    `json:"saldo"`
}
