package dto

import "time"

type Pengeluaran struct {
	Keperluan string    `json:"keperluan"`
	Jumlah    int       `json:"jumlah"`
	Tanggal   time.Time `json:"tanggal"`
}

// sort struct
type SlicePengeluaran []Pengeluaran

func (value SlicePengeluaran) Len() int {
	return len(value)
}

func (value SlicePengeluaran) Less(i, j int) bool {
	return value[i].Keperluan < value[j].Keperluan
}

func (value SlicePengeluaran) Swap(i, j int) {
	value[i], value[j] = value[j], value[i]
}

type UserPengeluaranRequest struct {
	Pengeluaran      []Pengeluaran `json:"pengeluaran"`
	TotalPengeluaran int           `json:"total_pengeluaran"`
}

type UserPengeluaranResponse struct {
	CatatanPengeluaran interface{} `json:"catatan_pengeluaran"`
	TotalPengeluaran   int         `json:"total_pengeluaran"`
}
