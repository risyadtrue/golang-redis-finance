package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"go-redis-finance/controller/dto"
	"go-redis-finance/helper"
	"go-redis-finance/repository"
	"net/http"
)

func GetSaldo(writer http.ResponseWriter, request *http.Request) {
	keys := chi.URLParam(request, "keys")

	redisDataSaldo, err := repository.RedisHGetAll(keys)
	if err != nil {
		return
	}

	var saldoUser, totalPemasukan, totalPengeluaran int
	for _, value := range redisDataSaldo {
		var dataSaldo dto.UserDataAll
		err = json.Unmarshal([]byte(value), &dataSaldo)
		if err != nil {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
			return
		}
		if dataSaldo.TotalPengeluaran != 0 {
			totalPengeluaran = dataSaldo.TotalPengeluaran
		} else {
			totalPemasukan = dataSaldo.TotalPemasukan
		}

	}

	saldoUser = totalPemasukan - totalPengeluaran

	saldoReponse := dto.UserSaloResponse{
		Nama:  keys,
		Saldo: saldoUser,
	}

	helper.HttpResponseSuccess(writer, request, saldoReponse)
}
