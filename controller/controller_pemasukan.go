package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-redis/redis"
	"go-redis-finance/controller/dto"
	"go-redis-finance/helper"
	"go-redis-finance/repository"
	"net/http"
	"sort"
	"time"
)

func BulkCreatePemasukan(writer http.ResponseWriter, request *http.Request) {
	keys := chi.URLParam(request, "keys")
	field := "pemasukan"

	var dataPemasukan dto.UserPemasukanRequest
	err := json.NewDecoder(request.Body).Decode(&dataPemasukan)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	var dataPemasukanInput int
	for i, data := range dataPemasukan.Pemasukan {
		dataPemasukan.Pemasukan[i].Tanggal = time.Now()
		dataPemasukanInput = dataPemasukanInput + data.Jumlah
	}

	dataPemasukan.TotalPemasukan = dataPemasukanInput

	var dataPemasukanRedis dto.UserPemasukanRequest
	err = repository.RedisHMGet(keys, field, &dataPemasukanRedis)
	if err != nil {
		if err == redis.Nil {
			// harusnya connect ke database
			mapPemasukan := make(map[string]interface{})
			dataByes, err := json.Marshal(dataPemasukan)
			mapPemasukan[field] = string(dataByes)
			err = repository.RedisHMSet(keys, mapPemasukan, time.Minute*10)
			if err != nil {
				helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
				return
			}
		}
	}

	for _, data := range dataPemasukan.Pemasukan {
		dataPemasukanRedis.Pemasukan = append(dataPemasukanRedis.Pemasukan, data)
	}

	sort.Sort(dto.SlicePemasukan(dataPemasukanRedis.Pemasukan))

	var totalPemasukan int
	for _, data := range dataPemasukanRedis.Pemasukan {
		totalPemasukan = totalPemasukan + data.Jumlah
	}

	dataPemasukanRedis.TotalPemasukan = totalPemasukan

	mapPemasukan := make(map[string]interface{})
	dataByes, err := json.Marshal(dataPemasukanRedis)
	mapPemasukan[field] = string(dataByes)
	err = repository.RedisHMSet(keys, mapPemasukan, time.Minute*10)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(writer, request, dataPemasukan)
}

func GetPemasukan(writer http.ResponseWriter, request *http.Request) {
	keys := chi.URLParam(request, "keys")
	field := "pemasukan"

	var dataPemasukan dto.UserPemasukanRequest
	err := repository.RedisHMGet(keys, field, &dataPemasukan)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	mapTipePemasukan := make(map[string]interface{})
	var dataTipePemasukan dto.UserPemasukanRequest
	for _, data := range dataPemasukan.Pemasukan {
		if _, found := mapTipePemasukan[data.Tipe]; !found {
			dataTipePemasukan = dto.UserPemasukanRequest{}
		}

		dataTipePemasukan.Pemasukan = append(dataTipePemasukan.Pemasukan, data)

		mapTipePemasukan[data.Tipe] = dataTipePemasukan.Pemasukan
	}

	reponsePemasukan := dto.UserPemasukanResponse{
		CatatanPemasukan: mapTipePemasukan,
		TotalPemasukan:   dataPemasukan.TotalPemasukan,
	}

	helper.HttpResponseSuccess(writer, request, reponsePemasukan)
}

//func BulkCreatePemasukanV2(writer http.ResponseWriter, request *http.Request) {
//	keys := chi.URLParam(request, "keys")
//	field := "pemasukan"
//
//	var dataPemasukan dto.BulkUserPemasukanRequest
//	err := json.NewDecoder(request.Body).Decode(&dataPemasukan)
//	if err != nil {
//		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
//		return
//	}
//
//	for i, _ := range dataPemasukan.Pemasukan {
//		dataPemasukan.Pemasukan[i].Tanggal = time.Now()
//	}
//
//	sort.Sort(dto.SlicePemasukan(dataPemasukan.Pemasukan))
//
//	mapPemasukan := make(map[string]interface{})
//	mapTipePemasukan := make(map[string]interface{})
//	var dataTipePemasukan dto.BulkUserPemasukanRequest
//
//	for _, value := range dataPemasukan.Pemasukan {
//		fmt.Println(value)
//		if _, found := mapTipePemasukan[value.Tipe]; !found {
//			dataTipePemasukan = dto.BulkUserPemasukanRequest{}
//		}
//
//		dataTipePemasukan.Pemasukan = append(dataTipePemasukan.Pemasukan, value)
//		mapTipePemasukan[value.Tipe] = dataTipePemasukan.Pemasukan
//	}
//
//	dataByes, err := json.Marshal(mapTipePemasukan)
//	mapPemasukan[field] = string(dataByes)
//	err = repository.RedisHMSet(keys, mapPemasukan, time.Minute*10)
//	if err != nil {
//		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
//		return
//	}
//
//	helper.HttpResponseSuccess(writer, request, dataPemasukan)
//}

//func CreatePemasukan(writer http.ResponseWriter, request *http.Request) {
//	keys := chi.URLParam(request, "keys")
//	field := "pemasukan"
//
//	var dataPemasukan dto.UserPemasukanRequest
//	err := json.NewDecoder(request.Body).Decode(&dataPemasukan)
//	if err != nil {
//		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
//		return
//	}
//
//	dataPemasukan.Nama = keys
//	dataPemasukan.Pemasukan.Tanggal = time.Now()
//
//	mapPemasukan := make(map[string]interface{})
//	dataByes, err := json.Marshal(dataPemasukan.Pemasukan)
//	mapPemasukan[field] = string(dataByes)
//	err = repository.RedisHMSet(keys, mapPemasukan, time.Minute*10)
//	if err != nil {
//		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
//		return
//	}
//
//	helper.HttpResponseSuccess(writer, request, dataPemasukan)
//}
