package routes

import (
	"fmt"
	"github.com/go-chi/chi"
	"go-redis-finance/controller"
	"log"
	"net/http"
)

func RouterCollection() {
	r := chi.NewRouter()
	r.Route("/api/v1", func(r chi.Router) {
		//r.Post("/pemasukan/{keys}", controller.CreatePemasukan)
		r.Post("/pemasukan/{keys}/all", controller.BulkCreatePemasukan)
		r.Get("/pemasukan/{keys}", controller.GetPemasukan)
		r.Post("/pengeluaran/{keys}/all", controller.BulkCreatePengeluaran)
		r.Get("/pengeluaran/{keys}", controller.GetPengeluaran)
		r.Get("/saldo/{keys}", controller.GetSaldo)
	})

	//r.Route("/api/v2", func(r chi.Router) {
	//	r.Post("/catatan/{keys}/{field}/all", controller.BulkCreatePemasukanV2)
	//})

	fmt.Println("Running in port localhost:3000")
	log.Fatal(http.ListenAndServe(":3000", r))
}
