package connection

import (
	"github.com/go-redis/redis"
)

func RedisConn() (client *redis.Client) {
	client = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})

	//pong, err := client.Ping().Result()
	//if err != nil {
	//	panic(err)
	//} else {
	//	fmt.Println(pong)
	//}

	return
}
