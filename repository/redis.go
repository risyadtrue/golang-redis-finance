package repository

import (
	"encoding/json"
	"go-redis-finance/connection"
	"time"
)

func RedisHMSet(keys string, data map[string]interface{}, expired time.Duration) (err error) {
	conn := connection.RedisConn()
	err = conn.HMSet(keys, data).Err()
	if err != nil {
		return err
	}

	conn.Expire(keys, expired)

	return
}

func RedisHMGet(keys string, field string, data interface{}) (err error) {
	conn := connection.RedisConn()
	result, err := conn.HGet(keys, field).Result()
	if err != nil {
		return err
	}
	//fmt.Println(result)

	err = json.Unmarshal([]byte(result), &data)
	if err != nil {
		return err
	}

	return
}

func RedisHGetAll(keys string) (result map[string]string, err error) {
	conn := connection.RedisConn()
	result, err = conn.HGetAll(keys).Result()
	if err != nil {
		return
	}

	return
}
